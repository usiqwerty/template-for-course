from django.db import models


class User(models.Model):
	name = models.TextField(max_length=32)
	telegram_id = models.IntegerField()
